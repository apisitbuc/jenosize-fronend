import RestaurantSearch from "@/components/RestaurantSearch";
import TopBar from "@/components/TopBar";
import Body from "@/components/body";
import Footer from "@/components/footer";

const Home = () => {
  return (
    <div>
      <div>
        <TopBar />
        <Body>
          <RestaurantSearch />
        </Body>
        <Footer />
      </div>
    </div>
  );
};

export default Home;
