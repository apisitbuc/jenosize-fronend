import { List, Image, Row, Col } from "antd";

interface Restaurant {
  name: string;
  address: string;
}

interface RestaurantListProps {
  restaurants: Restaurant[]; //
}

const RestaurantList = ({ restaurants }: RestaurantListProps) => {
  return (
    <List
      dataSource={restaurants}
      renderItem={(restaurant) => (
        <List.Item>
          <List.Item>
            <Row>
              <Col span={8}>
                <Image
                  width={50}
                  preview={false}
                  src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                  alt="aaaa"
                />
              </Col>
              <Col span={12}>
                <h3>{restaurant.name}</h3>
                <p>{restaurant.address}</p>
              </Col>
            </Row>
          </List.Item>
        </List.Item>
      )}
    />
  );
};

export default RestaurantList;
