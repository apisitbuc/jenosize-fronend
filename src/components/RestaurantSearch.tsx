import { Col, Row } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import RestaurantList from "../components/RestaurantList";
import restaurantsData from "../../restaurant.json";

const RestaurantSearch = () => {
  const [restaurants, setRestaurants] = useState<any>([]);

  useEffect(() => {
    const fetchRestaurants = async () => {
      try {
        // const response = await axios.get(
        //   `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${query}&key=YOUR_API_KEY`
        // );
        const response = {
          data: restaurantsData,
        };
        setRestaurants(response.data.results);
      } catch (error) {
        console.error("Error fetching restaurant data:", error);
      }
    };

    fetchRestaurants();
  }, []);

  return (
    <Row>
      <Col span={12} offset={6}>
        <RestaurantList restaurants={restaurants} />
      </Col>
    </Row>
  );
};

export default RestaurantSearch;
