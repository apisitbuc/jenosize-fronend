/* eslint-disable jsx-a11y/alt-text */
import { Col, Layout, Row } from "antd";
import { Image } from "antd";
import SearchForm from "./SearchForm";

const { Header } = Layout;
import { Typography } from "antd";

const { Title, Text } = Typography;

const TopBar = () => {
  return (
    <Header>
      <Row>
        <Col span={8}>
          <Row>
            <Col span={6}>
              <Image
                width={50}
                preview={false}
                src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
              />
            </Col>
            <Col span={12}>
              <Title level={2} type="danger">
                Website Name
              </Title>
            </Col>
          </Row>
        </Col>
        <Col span={8} offset={8}>
          <Row>
            <Text type="warning">search</Text>
          </Row>
          <Row>
            <SearchForm />
          </Row>
        </Col>
      </Row>
    </Header>
  );
};

export default TopBar;
