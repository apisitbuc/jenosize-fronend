import { Layout } from "antd";

const { Footer } = Layout;

const AppFooter = () => {
  return <Footer>{`Footer`}</Footer>;
};

export default AppFooter;
