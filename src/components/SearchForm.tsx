import { Input } from "antd";

const { Search } = Input;

const SearchForm = () => {
  
  return <Search placeholder="search" size="small" allowClear />;
};

export default SearchForm;
