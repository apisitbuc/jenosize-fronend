import { Layout } from "antd";

const { Content } = Layout;
interface IBody {
  children: React.ReactNode;
}

const Body = (body: IBody) => {
  return <Content>{body.children}</Content>;
};

export default Body;
