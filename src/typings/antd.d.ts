import { CSSProperties } from 'react';

declare module 'antd' {
  export interface ButtonProps {
    // Add any custom props for Button component
    // Example: customProp?: string;
  }

  // Add any additional component declarations with custom props if needed
}

declare module 'antd/lib/style' {
  // Add any custom style declarations
}
